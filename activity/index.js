const getCube = 8 ** 3;
console.log(`The cube of 8 is ${getCube}`);

const address = ["258", "Washington Ave NW", "California", "90011"];

const [addressNumber, addressName, addressCountry, addressZipCode] = address;

console.log(
    `I live at ${addressNumber} ${addressName}, ${addressCountry}, ${addressZipCode}`
);

const animals = {
    name: "Lolong",
    species: "saltwater crocodile",
    weight: "1075 kgs",
    measurement: "20ft 3 in",
};
const {
    name: animeName,
    species: animalSpecies,
    weight: animalWeight,
    measurement: animalMeasurement,
} = animals;
console.log(`
${animeName} was a ${animalSpecies}. He weight at ${animalWeight} with a measurement of ${animalMeasurement}
`);

let numbers = [1, 2, 3, 4, 5];

numbers.forEach((number) => {
    console.log(number);
});
let sum = numbers.reduce((x, y) => x + y);
console.log(sum);

class dog {
    constructor(name, age, breed) {
        this.name = name;
        this.age = age;
        this.breed = breed;
    }
}
const myDog = new dog();
myDog.name = "Megan";
myDog.age = 2;
myDog.breed = "Shih Tzu";
console.log(myDog);
